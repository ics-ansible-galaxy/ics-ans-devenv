import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('devenv')


def test_devenv_setup_enabled(host):
    service = host.service("devenv-setup")
    assert service.is_enabled


def test_devenv_config(host):
    cmd = host.run('sudo /usr/local/bin/devenv-config -h')
    assert '--setup' in cmd.stdout
