import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("ics-ans-devenv-default")


def test_netbeans_installed(host):
    assert host.file("/opt/netbeans-8.2").exists


def test_scenebuilder_installed(host):
    assert host.file("/opt/SceneBuilder").exists


def test_archiver_appliance_running(host):
    assert host.service("archiver-appliance").is_running


def test_docker_not_installed(host):
    assert not host.package("docker-ce").is_installed


def test_archappl_access(archappl_index):
    assert "<title>appliance archiver - Home</title>" in archappl_index
