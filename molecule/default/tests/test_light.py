import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-devenv-light')


def test_netbeans_not_installed(host):
    assert not host.file('/usr/local/netbeans-8.2').exists
    assert not host.file('/opt/netbeans-8.2').exists


def test_scenebuilder_not_installed(host):
    assert not host.file('/opt/SceneBuilder').exists
