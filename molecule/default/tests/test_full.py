import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-devenv-full')


def test_docker_installed(host):
    assert host.package("docker-ce").is_installed


def test_notebook_index(host):
    cmd = host.command('curl -L http://localhost:8888')
    assert '<title>Jupyter Notebook</title>' in cmd.stdout
