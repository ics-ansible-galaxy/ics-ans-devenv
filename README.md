ics-ans-devenv
==============

Ansible playbook to install development environmnent machine.
The playbook is used in the ics-packer-devenv to create the VM and USB image.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
